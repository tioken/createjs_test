var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./base_controller", "../model/webrtc", "../view/webrtc"], function (require, exports, BC, WM, WV) {
    var WebrtcController = (function (_super) {
        __extends(WebrtcController, _super);
        function WebrtcController(data) {
            var _this = this;
            _super.call(this, new WM.WebrtcModel(data), new WV.WebrtcView());
            this.connection_list = {};
            var nv = navigator;
            nv.getUserMedia = nv.getUserMedia || nv.webkitGetUserMedia || nv.mozGetUserMedia;
            nv.getUserMedia({ audio: true, video: false }, function (stream) {
                _this.my_stream = stream;
                _this.createSession();
            }, function (err) {
                console.log('Failed to get local stream', err);
            });
        }
        WebrtcController.prototype.createSession = function () {
            var _this = this;
            this.socket = io.connect('http://bucket.gift:3000', {
                'reconnect': true,
                'reconnection delay': 500,
                'max reconnection attempts': 10
            });
            this.peer = new Peer({ key: '075c2c3d-7324-4685-b144-01ecf8b1580f' });
            this.socket.on('connect', function (msg) {
                _this.peer.on('open', function (id) {
                    _this.my_id = id;
                    _this.socket.emit('connected', { room_id: "room", user_id: id, name: id });
                    _this.socket.emit('get-user-list');
                });
            });
            this.socket.on('reconnect', function (msg) {
                _this.removeVoiceElementAll();
                _this.socket.emit('connected', { room_id: "room", user_id: _this.my_id, name: _this.my_id });
                _this.socket.emit('get-user-list');
            });
            this.socket.on('connected', function (data) {
                console.log(data.name + "が入室しました。");
            });
            this.socket.on('disconnected', function (data) {
                console.log(data.name + "が退室しました。");
                _this.removeVoiceElement(data.user_id);
            });
            this.socket.on('user-list', function (data) {
                console.log(data);
                Object.keys(data).forEach(function (key) {
                    if (data[key].user_id != _this.my_id) {
                        var conn = _this.peer.connect(data[key].user_id);
                        conn.on('open', function () {
                            _this.connection_list[conn.peer] = conn;
                            conn.send('Hello!');
                        });
                        conn.on("data", _this.onRecvMessage);
                        var call = _this.peer.call(data[key].user_id, _this.my_stream);
                        call.on('stream', function (stream) {
                            _this.addVoiceElement(stream, data[key].user_id);
                        });
                    }
                });
            });
            $("#send-btn").on('click', function () {
                Object.keys(_this.connection_list).forEach(function (key) {
                    _this.connection_list[key].send($("#text").val());
                });
            });
            this.peer.on('connection', function (conn) {
                if (!_this.connection_list[conn.peer]) {
                    _this.connection_list[conn.peer] = conn;
                    conn.on("open", function () {
                        console.log(conn.id);
                    });
                    conn.on("data", _this.onRecvMessage);
                }
            });
            this.peer.on('error', function (e) {
                console.log(e);
            });
            this.peer.on('call', function (call) {
                call.answer(_this.my_stream);
                call.on('stream', function (stream) {
                    _this.addVoiceElement(stream, call.peer);
                });
            });
        };
        WebrtcController.prototype.onRecvMessage = function (data) {
            console.log(data);
        };
        WebrtcController.prototype.addVoiceElement = function (stream, user_id) {
            var container = $('<div class="voice-item" id="user-' + user_id + '"></div>');
            var video = $('<video class="voice" autoplay="true"/>').prop('src', URL.createObjectURL(stream));
            var name = $('<div class="name">ユーザー名<br>' + user_id + '</div>');
            container.append(video).append(name);
            $("#voice-area").append(container);
        };
        WebrtcController.prototype.removeVoiceElement = function (user_id) {
            $('#user-' + user_id).remove();
        };
        WebrtcController.prototype.removeVoiceElementAll = function () {
            $('.voice-item').remove();
        };
        return WebrtcController;
    })(BC.BaseController);
    exports.WebrtcController = WebrtcController;
});
