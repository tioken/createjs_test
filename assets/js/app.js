// socket.ioの読み込み
var conn = require("socket.io")(3000, {
    key : fs.readFileSync('/etc/pki/tls/private/your.domain.com.key').toString(),
    cert: fs.readFileSync('/etc/pki/tls/certs/your.domain.com.crt').toString(),
    ca: fs.readFileSync('/etc/pki/tls/certs/your.domain.com.cer').toString(),
});
var redis = require('socket.io-redis');
var adapter = conn.adapter(redis({ host: 'localhost', port: 6379 }));

var clientHash = {};
var roomList = {};

conn.sockets.on("connection", function (socket) {
    // クライアントからのデータの受信
    socket.on("connected", function(data) {
        clientHash[socket.id] = data;
        socket.join(data.room_id);

        if(typeof roomList[data.room_id] === "undefined"){
            roomList[data.room_id] = {user_list:{}};
        }

        socket.broadcast.to(data.room_id).emit("joined", data);
        socket.emit("connected", data);

        //ルームにユーザーを追加
        roomList[data.room_id].user_list[data.user_id] = data;

        //console.log(roomList[data.room_id]);
    });
    // クライアントからのデータの受信
    socket.on("message", function(data) {
        if(!clientHash[socket.id])return false;

        socket.to(clientHash[socket.id].room_id).emit("message", data);
        socket.emit("message", data);
    });
    // ルームに接続しているユーザーの一覧を返す
    socket.on("get-user-list", function(data) {
        if(!clientHash[socket.id])return false;

        var hogehoge = roomList[clientHash[socket.id].room_id].user_list;

        socket.emit("user-list", hogehoge);
        console.log(hogehoge);
    });
    // 接続終了組み込みイベント(接続元ユーザを削除し、他ユーザへ通知)
    socket.on("disconnect", function () {
        if(!clientHash[socket.id])return false;

        socket.to(clientHash[socket.id].room_id).emit("disconnected", clientHash[socket.id]);

        delete roomList[clientHash[socket.id].room_id].user_list[clientHash[socket.id].user_id];

        console.log("disconnect:"+clientHash[socket.id].user_id);
        //console.log(roomList[clientHash[socket.id].room_id]);
        clientHash[socket.id] = null;
    });
});