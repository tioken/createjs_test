var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "../model/base_model"], function (require, exports, bm) {
    var HomeModel = (function (_super) {
        __extends(HomeModel, _super);
        function HomeModel(data) {
            _super.call(this);
            this.url = {};
            this.loadParam(data);
        }
        return HomeModel;
    })(bm.BaseModel);
    exports.HomeModel = HomeModel;
});
