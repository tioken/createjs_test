var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
define(["require", "exports", "./base_view"], function (require, exports, BV) {
    var WebrtcView = (function (_super) {
        __extends(WebrtcView, _super);
        function WebrtcView() {
            _super.call(this);
            this.init();
        }
        WebrtcView.prototype.init = function () {
            var canvas = $("#canvas").get(0);
        };
        return WebrtcView;
    })(BV.BaseView);
    exports.WebrtcView = WebrtcView;
});
