define(["require", "exports"], function (require, exports) {
    var TSRouter = (function () {
        function TSRouter() {
        }
        TSRouter.getController = function (param) {
            var router = new Router();
            router.route('/createjs_test', function () {
                require(['controller/home'], function (controller) {
                    return new controller.HomeController(param);
                });
            });
            router.route('/createjs_test/webrtc.html#:id', function (id) {
                require(['controller/webrtc'], function (controller) {
                    console.log(id);
                    return new controller.WebrtcController(param);
                });
            });
            router.start();
            return true;
        };
        return TSRouter;
    })();
    exports.TSRouter = TSRouter;
    var jupiter_config = jupiter_config || {};
    TSRouter.getController(jupiter_config);
});
