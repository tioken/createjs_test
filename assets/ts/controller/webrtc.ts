///<reference path="../d.ts/peerjs/peerjs.d.ts" />
///<reference path="../d.ts/socket.io-client/socket.io-client.d.ts" />

import  BC = require("./base_controller");
import  WM = require("../model/webrtc");
import  WV = require("../view/webrtc");

export class WebrtcController extends BC.BaseController<WM.WebrtcModel, WV.WebrtcView>{

    connection_list:any;
    socket:SocketIOClient.Socket;
    my_id:number;
    peer:PeerObject;
    my_stream:any;

    constructor(data:any) {
        super(new WM.WebrtcModel(data), new WV.WebrtcView());

        this.connection_list = {};


        //自分のストリームを準備
        var nv = <any>navigator;
        nv.getUserMedia = nv.getUserMedia || nv.webkitGetUserMedia || nv.mozGetUserMedia;

        nv.getUserMedia({audio: true, video: false}, (stream)=>{
            this.my_stream = stream;
            this.createSession();
        }, function(err){
            console.log('Failed to get local stream' ,err);
        });


    }
    createSession():void{
        //milkcocoaのデータストアを呼び出し
        this.socket = io.connect('http://bucket.gift:3000',{
            'reconnect': true,
            'reconnection delay': 500,
            'max reconnection attempts': 10
        });

        this.peer = new Peer({key: '075c2c3d-7324-4685-b144-01ecf8b1580f'});

        this.socket.on('connect', (msg)=> {
            this.peer.on('open', (id)=>{
                this.my_id = id;
                this.socket.emit('connected', { room_id:"room", user_id: id, name:id });
                this.socket.emit('get-user-list');
            });
        });
        this.socket.on('reconnect', (msg)=> {
            this.removeVoiceElementAll();
            this.socket.emit('connected', { room_id:"room", user_id: this.my_id, name:this.my_id });
            this.socket.emit('get-user-list');
        });
        //ユーザーが入室したとき
        this.socket.on('connected', (data) => {
            // メッセージを画面に表示する
            console.log(data.name + "が入室しました。");
        });

        // ユーザーが退室したとき
        this.socket.on('disconnected', (data) => {
            // メッセージを画面に表示する
            console.log(data.name + "が退室しました。");
            this.removeVoiceElement(data.user_id);
        });

        // ユーザーリスト取得時
        this.socket.on('user-list', (data) => {
            // メッセージを画面に表示する
            console.log(data);

            //自分以外の相手とのP2P接続を確立
            Object.keys(data).forEach((key) => {
                if(data[key].user_id !=this.my_id) {
                    var conn = this.peer.connect(data[key].user_id);
                    conn.on('open',  () => {
                        this.connection_list[conn.peer] = conn;
                        // メッセージを送信
                        conn.send('Hello!');
                    });
                    // メッセージ受信イベントの設定
                    conn.on("data", this.onRecvMessage);

                    var call = this.peer.call(data[key].user_id, this.my_stream);
                    call.on('stream', (stream) => {
                        this.addVoiceElement(stream, data[key].user_id);
                    });
                }
            })
        });

        $("#send-btn").on('click', ()=>{
            //全員にテキストをP2Pで送る
            Object.keys(this.connection_list).forEach((key) => {
                this.connection_list[key].send($("#text").val());
            });
        });


        this.peer.on('connection', (conn)=> {
            if (!this.connection_list[conn.peer]) {
                this.connection_list[conn.peer] = conn;

                // 接続が完了した場合のイベントの設定
                conn.on("open", function() {
                    // 相手のIDを表示する
                    // - 相手のIDはconnectionオブジェクトのidプロパティに存在する
                    console.log(conn.id);
                });

                // メッセージ受信イベントの設定
                conn.on("data", this.onRecvMessage);
            }
        });
        this.peer.on('error', function(e) {
            console.log(e);
        });

        this.peer.on('call', (call) => {
            // Answer the call, providing our mediaStream
            call.answer(this.my_stream);
            call.on('stream', (stream)=>{
                // メディアデータ受信処理
                this.addVoiceElement(stream, call.peer);
            });
        });
    }
    onRecvMessage(data):void {
        // 画面に受信したメッセージを表示
        console.log(data);
    }
    addVoiceElement(stream, user_id):void{
        var container = $('<div class="voice-item" id="user-'+user_id+'"></div>');
        var video = $('<video class="voice" autoplay="true"/>').prop('src', URL.createObjectURL(stream));
        var name = $('<div class="name">ユーザー名<br>'+user_id+'</div>');
        container.append(video).append(name);
        $("#voice-area").append(container);
    }
    removeVoiceElement(user_id):void{
        $('#user-'+user_id).remove();
    }
    removeVoiceElementAll():void{
        $('.voice-item').remove();
    }
}