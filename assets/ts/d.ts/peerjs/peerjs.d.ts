interface DataConnection {
    send(data: any): void;
    close(): void;
    on(event: string, callback: (p?: any) => void): void;
    bufferSize: number;
    dataChannel: Object;
    label: string;
    metadata: any;
    open: boolean;
    peerConnection: Object;
    peer: string;
    reliable: boolean;
    serialization: string;
    type: string;
}

interface MediaConnection {
    answer(stream?: any): void;
    close(): void;
    on(event: string, callback: (p?: any) => void): void;
    open: boolean;
    metadata: any;
    peer: string;
    type: string;
}

interface ConnectOptions {
    label?: string;
    metadata?: any;
    serialization?: string;
    reliable?: boolean;
}

interface PeerObject {
    connect(id: string, options?: ConnectOptions): DataConnection;
    call(id: string, stream: any): MediaConnection;
    on(event: string, callback: (p?: any) => void): void;
    disconnect(): void;
    destroy(): void;
    id: string;
    connections: Object;
    disconnected: boolean;
    destroyed: boolean;
}

interface PeerOptions {
    key?: string;
    host?: string;
    port?: number;
    path?: string;
    secure?: boolean;
    config?: Object;
    debug?: number;
}

interface PeerStatic {
    new(id: string, options?: PeerOptions) : PeerObject;
    new(options: PeerOptions) : PeerObject;
}

interface SupportsData {
    audioVideo: boolean;
    data: boolean;
    binary: boolean;
    reliable: boolean;
}

interface UtilStatic {
    browser: string;
    supports: SupportsData;
}

declare var Peer: PeerStatic;
declare var util: UtilStatic;