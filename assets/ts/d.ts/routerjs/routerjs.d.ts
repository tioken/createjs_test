declare module routerjs{
    export class Router{
        constructor(router?:Object);
        route(string:string, callback?):void;
        start():void;
    }
}
import  Router = routerjs.Router;