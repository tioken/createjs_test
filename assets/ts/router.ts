///<reference path="d.ts/routerjs/routerjs.d.ts" />
declare var jupiter_config:any;
declare var require;

//これは必要無いけどコンパイル時にファイルが参照されるように
import  c1 = require("controller/home");
import  c2 = require("controller/webrtc");

export class TSRouter {
    static getController(param:any){
        var router = new Router();
        router.route('/createjs_test', ()=>{
            require(['controller/home'], (controller)=>{
                return new controller.HomeController(param);
            });
        });
        router.route('/createjs_test/webrtc.html#:id', (id)=>{
            require(['controller/webrtc'], (controller)=>{
                console.log(id);
                return new controller.WebrtcController(param);
            });
        });
        router.start();
        return true;
    }
}
var jupiter_config = jupiter_config || {};
TSRouter.getController(jupiter_config);
